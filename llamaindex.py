import pandas as pd
# from llama_index.query_engine import PandasQueryEngine

from llama_index import ServiceContext
from llama_index.embeddings.gemini import GeminiEmbedding
from llama_index.llms.gemini import Gemini


def create_embedding(gemini_api_token):
    embedding = GeminiEmbedding(model_name="models/embedding-001", api_key=gemini_api_token)
    return embedding


def create_llm(gemini_api_token):
    llm_model = Gemini(api_key=gemini_api_token)
    return llm_model


def create_service_context(embedding, llm_model):
    service_context = ServiceContext.from_defaults(
    embed_model=embedding, llm=llm_model)
    return service_context


# def create_query_engine(df, service_context):
#     query_engine = PandasQueryEngine(df=df, verbose=True, service_context=service_context)
#     return query_engine


# def create_query(query_engine, query_str):
#     response = query_engine.query(query_str)
#     return response