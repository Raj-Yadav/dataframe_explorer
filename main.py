import gradio as gr
import pandas as pd
from query_engine import create_query_engine, create_query


def read_csv_data(csv_url):
    return pd.read_csv(csv_url)

with gr.Blocks() as demo:
    gr.Markdown("Start querying your csv file")

    with gr.Row():
        with gr.Column():
            with gr.Group():
                api_token = gr.Textbox(placeholder="GEMINI_API_TOKEN", type="password")
                in_file = gr.File()
                btn = gr.Button("Load Data")
                # btn.click(fn=read_csv_data, inputs=in_file, outputs=out_file)

            with gr.Group():
                df = gr.Dataframe()
                btn.click(fn=read_csv_data, inputs=in_file, outputs=df)

        with gr.Column():
            query_engine_btn = gr.Button("Create Query Engine")
            query_engine_state = gr.State([])
            query_engine_btn.click(fn=create_query_engine, inputs=[df, api_token], outputs=query_engine_state)

            question = gr.TextArea(label="Questions" , placeholder="ask questions related to your datasets")
            query_btn = gr.Button("Submit")
            answer = gr.TextArea(label="Answers")
            query_btn.click(fn=create_query, inputs=[query_engine_state, question], outputs=[answer])


demo.launch()
    