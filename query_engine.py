from llama_index.query_engine import PandasQueryEngine
from llama_index import ServiceContext
from llamaindex import create_embedding, create_llm, create_service_context
from JupyterFolder.config import GEMINI_API_TOKEN

# EMBEDDING = create_embedding(gemini_api_token=GEMINI_API_TOKEN)
# LLM_MODEL = create_llm(gemini_api_token=GEMINI_API_TOKEN)

# service_context = create_service_context(embedding=EMBEDDING, llm_model=LLM_MODEL)

def create_query_engine(df, GEMINI_API_TOKEN):
    EMBEDDING = create_embedding(gemini_api_token=GEMINI_API_TOKEN)
    LLM_MODEL = create_llm(gemini_api_token=GEMINI_API_TOKEN)
    service_context = create_service_context(embedding=EMBEDDING, llm_model=LLM_MODEL)

    query_engine = PandasQueryEngine(df=df, verbose=True, service_context=service_context)
    return query_engine


def create_query(query_engine, query_str):
    response = query_engine.query(query_str)
    return response